package carddeck.carddeck;

/**
 * This is the domain class that represents a card in a card game.
 */
public class Card {
	
	private Rank rank;
	private Suit suit;
	
	public Card (Rank rank, Suit suit) {
		this.rank = rank;
		this.suit = suit;
	}
	
	public Suit getSuit() {
		return this.suit;
	}
	
	public Rank getRank() {
		return this.rank;
	}
	
	public String toString() {
		return  "Rank - " + getRank() + ", Suit - " + getSuit();
	}
	
	@Override
	public boolean equals(Object obj) {
		return  ((Card)obj).rank == this.rank && ((Card)obj).suit == this.suit;
	}
}
