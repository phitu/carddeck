package carddeck.carddeck;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * This is the class that represents a deck of cards in a card game.
 */
public class Deck implements IDeck {
	
	private List<Card> unshuffledDeck = new ArrayList<Card>();
	private List<Card> shuffledDeck = new ArrayList<Card>();
	
	public List<Card> getDeck() {
		return unshuffledDeck;
	}
	
	/**
	 * create deck of cards
	 */
	private void createDeck() {
		for(Rank rank : Rank.values()) {
			for(Suit suit : Suit.values()) {				
				unshuffledDeck.add(new Card(rank, suit));
			}
		}
	}
	
	/**
	 * shuffle the deck of cards in random order	
	 */
	public void shuffleDeck() {
		unshuffledDeck.clear();
		shuffledDeck.clear();
		createDeck();
		List<Card> cards = getDeck();
		Random randomGenerator = new Random();
		int deckSize = unshuffledDeck.size();
		for (int i =0; i<deckSize; i++) {
			int index = randomGenerator.nextInt(cards.size());
			Card card = cards.get(index);
			cards.remove(card);
			getShuffledDeck().add(card);
		}
	}

	public List<Card> getShuffledDeck() {
		return shuffledDeck;
	}
	
	/**
	 * draw top card from deck
	 * @return
	 */
	public Card drawCardFromDeck() {
		Card card = shuffledDeck.get(0);
		shuffledDeck.remove(card);
		return card;
	}
	
}
