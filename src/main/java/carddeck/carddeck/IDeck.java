package carddeck.carddeck;

public interface IDeck {

	void shuffleDeck();
	
	Card drawCardFromDeck();
	
}
