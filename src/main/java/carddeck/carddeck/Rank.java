package carddeck.carddeck;

/**
 * This is the domain class that represents a card in a card game.
 */
public enum Rank {
	ACE("A"), TWO("Two"), THREE("Three"), FOUR("four"), FIVE("Five"), SIX("Six"), SEVEN("Seven")
	, EIGHT("Eight"), NINE("Nine"), TEN("Ten"), JACK("Jack"), QUEEN("Queen"), KING("King");
	
	private String rank;
	
	Rank (String rank) {
		this.rank = rank;
	}
	
	public String getValue() {
		return this.rank;
	}
	
}
