package carddeck.carddeck;

public enum Suit {
	SPADE("Spade"), HEART("Heart"), CLUB("Club"), DIAMOND("Diamond");
	
	private String suit;
	
	Suit(String suit) {
		this.suit = suit;
	}
	
	public String getValue() {
		return suit;
	}
}
