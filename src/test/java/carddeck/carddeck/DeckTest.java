package carddeck.carddeck;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class DeckTest {

	private Deck deck;
	
	@Before
	public void setup() {
		deck = new Deck();
	}
	
	@Test
	public void deckOfCardsReturn52() {
		deck.shuffleDeck();
		Set<Card> cardDeckSet = new HashSet<Card>(deck.getShuffledDeck());		
		
		assertThat(cardDeckSet.size(), is(52));
	}
	
	@Test
	public void shuffleDeckTestFirstCardNotDrawnAgain() {
		deck.shuffleDeck();
		Card c = deck.getShuffledDeck().get(0);
		int deckShuffleCount = 50;
		int count =0;
		for(int i=0; i<deckShuffleCount; i++) {
			deck.shuffleDeck();
			Card c2= deck.getShuffledDeck().get(0);
			if(c2.equals(c)) {
				count++;
			}
		}
		assertFalse(count==deckShuffleCount);
	}
	
	@Test
	public void drawCardFromDeckDecrementsDeckCount() {
		deck.shuffleDeck();
		deck.drawCardFromDeck();
		assertThat(deck.getShuffledDeck().size(), is(51));
	}

	@Test
	public void drawCardFromDeckReshuffleRestoresDeckCountTo52() {
		deck.shuffleDeck();
		deck.drawCardFromDeck();
		deck.drawCardFromDeck();
		deck.drawCardFromDeck();
		assertThat(deck.getShuffledDeck().size(), is(49));
		deck.shuffleDeck();
		assertThat(deck.getShuffledDeck().size(), is(52));
	}
	
}
